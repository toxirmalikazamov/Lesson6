const App = document.getElementById("app");
const colorChange = document.getElementById("buttonChanger");
const buttons = document.getElementsByClassName("button");

colorChange.addEventListener("click", function (e) {
  Array.from(buttons).forEach((buttons) => {
    if (buttons === e.target) {
      var bodyColor = e.target.style.backgroundColor;
      App.style.backgroundColor = bodyColor;
    }
  });
});
